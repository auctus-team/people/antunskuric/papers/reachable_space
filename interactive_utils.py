import numpy as np
import time

import roboticstoolbox as rp

# polytope python module
import pycapacity.algorithms as algos
import tkinter as tk

class RobotSlider:
    def __init__(self,robot,env,callback = None):
        self.root = tk.Tk()
        self.env = env
        self.robot = robot
        self.callback = callback
        
        self.sliders = []
        for i, q in enumerate(robot.qlim.T):
            w = tk.Scale(self.root, from_=q[1], to=q[0], resolution=0.01, orient=tk.HORIZONTAL)#, command= self.updateValue )
            self.sliders.append(w)
            w.set(robot.q[i])
            w.pack()
            w.bind("<ButtonRelease-1>", self.updateValue)
            
        self.root.mainloop()

    def updateValue(self, event):
        for i, slider in enumerate(self.sliders):
            self.robot.q[i] = slider.get() 
        if self.callback :
                self.callback()